package com.example.spring.security.springsecurity.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloResource {
	
	// accessible : all 
	@GetMapping("/")
	public String main() {
		return "static page display - welcome";
	}
	
	// accessible : user + admin
	@GetMapping("/user")
	public String user() {
		return "user module";
	}
	
	// accessible : admin
	@GetMapping("/admin")
	public String admin() {
		return "admin module";
	}
}
