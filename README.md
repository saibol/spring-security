# Spring Security

It is an application framework that help you do application level security.

## Benefits 
1. Application security
	- Login and logout functionality
	- Allow/block access to URL's to logged in users
	- Allow/block access to URL's to logged in users AND with certain roles
2. Flexible and Customizable 
3. Handles common vulnerabilities
	- Session fixation
	- Click Jacking
	- Click Site Request Forgery (CSRF)
4. Can do : 
	- User Name / password authentication 
	- SSO / Okta / LDAP
	- App Level Authorization 
	- Intra App Authorization like OAuth
	- Microservice Security (using tokens, JWT)
	- Method level security

## 5 Core Concepts in Spring Security
1. **Authentication** (who are you)
-	knoledge based authencation : knowledge that real user have 
	*example* : password, pincode, answer to secret question / personal question 
-	Possession based authencation : Are you in possession of something that real user should be in possession of. 
	*example* : phone/text message, key cards and badges, access token devices 
-	 Multifactor Authentication : combination of knowledge based and possession based authentication  

2. **Authorization** (what do you want)
	Making decision whether or not, a user can do particular operation. 
	
3. **Principal** : Currently logged in user 
4. **Granted Authority** : permissions 
5. **Roles** : group of permissions

## Sample Application 

- Visit [https://start.spring.io](https://start.spring.io/) > Project : Maven > Language : Java > Dependencies : spring web, spring security > Click 'Generate'
- Import the maven project in eclipse and run
- application will run on port 8080
  **Note** : In login form, username is 'user'. 
    password : every time spring application starts, it create default password. You can find this generated password in log
 - To give customized username password, make below change in *application.properties* file

```text 
spring.security.user.name=foo
spring.security.user.password=foo
```	

## Spring Security Default Behavior
- Add mandatory authentication for all URLs
- Adds login form 
- Handles login error
- Creates a user and sets a deafult password


## Configure Spring Security Authentication

**Step 1** : Get hold of AuthenticationManagerBuilder by extends  WebSecurityConfigurerAdapter and override configure(AuthenticationManagerBuilder)

**Step 2** : Set the configuration on it

```java

@RestController
public class HelloResource {
	
	// accessible : all 
	@GetMapping("/")
	public String main() {
		return "static page display - welcome";
	}
	
	// accessible : user + admin
	@GetMapping("/user")
	public String user() {
		return "user module";
	}
	
	// accessible : admin
	@GetMapping("/admin")
	public String admin() {
		return "admin module";
	}
}

@EnableWebSecurity
class SecurityConfigurer extends WebSecurityConfigurerAdapter{
	
	/**
	* By overriding this configure method, we are getting hold to {@AuthenticationManagerBuilder},
	* which we then used to provide list of allowed users. 
	*/
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// Setting the configuration on it using in memory authentication 
		auth.inMemoryAuthentication()
		.withUser("blah")
		.password("blah")
		.roles("USER");
	}
	
	/**
	* This method indicates which password encoding is used. 
	* With NoOpPasswordEncoder, there is no encoding used i.e. plain String text is returned 
	* @returns {@PasswordEncoder}
	*/
	@Bean
	public PasswordEncoder getPasswordEncoder(){
		return NoOpPasswordEncoder.getInstance();	
	}
}
```

**Note** : Please find the complete source code for above code at :  
https://bitbucket.org/saibol/spring-security/src/master/

## Configure Spring Security Authorization 

**Step 1** : Get hold of HttpSecurity by extending  WebSecurityConfigurerAdapter and override configure(HttpSecurity)

**Step 2** Set the configuration on it using authorizeRequests()

```java
@EnableWebSecurity
class SecurityConfigurer extends WebSecurityConfigurerAdapter{
	
	/**
	* Assumption : You have already made authentication code changes
	*/

	/**
	* By overriding this configure method, we are getting hold to {@HttpSecurity},
	* which we then used to restrict users based on their roles. 
	* Here while setting antMatchers, we set access from most restricted to less restrictive
	*/
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers("/admin").hasAnyRole("ADMIN")
		.antMatchers("/user").hasAnyRole("USER")
		.antMatchers("/").permitAll()
		.and().formLogin();
	}
}
```
## Spring Security Internal working 

- Sprint Security internally uses servlet filters to intercept requests.
- It even can stop the request so it don't reach the servlet.
- When *sprint-starters-security* is added to project, it does filter mapping to intercept all the request and maps it with spring securities own filter **DelegatingFilterProxy**. 
```xml
<filter>
	<filter-name>springSecurityFilterChain</filter-name>
	<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
</filter>
<filter-mapping>
	<filter-name>springSecurityFilterChain</filter-name>
	<url-pattern>/*</url-pattern>
</filter-mapping>
``` 

- DelegatingFilterProxy delegate the request to other spring security specific filters (one of such filter is authencation filter)

![Spring Security Authentication Internal Working](./assets/sprint-security-authentication-working.jpeg)

### How Authentication is Done ?
- AuthenticationFilter intercepts AuthenticationRequests. It creates Authentication object with cridentials and passes it to AuthenticationManager.
- AuthenticationManager then finds the appropriate AuthenticationProvider using supports() method 
- AuthenticationManager calls authencation() of respective AuthenticationProvider and passes Authentication Object
- AuthenticationProvider looks up corresponding user in the system using UserDetailsService.
- UserDetailsService returns the UserDetails instance which the authenticate() method then verifies 
- if authencation is successful, Authentication object is sent along with Principal (i.e. Principal is instance of UserDetails object)
- Principal is then put into Security Context in ThreadLocal object which is used later for authorization and authenticating subsequent requests
- Then this Authentication object goes back all to Authentication Filter which started all
- if authencation is not successful, AuthenticationProvider throws an AuthenticationException which is sent to AuthenticationFilter


## How to setup JDBC authentication with Spring Security

**Step 1**: create sample application with spring security having basic spring authencation and authorization implemented
(Source code for basic code : https://bitbucket.org/saibol/spring-security/src/master/)
**Step 2**: As with this example we creating in memory h2 database. add below dependencies in pom.xml
```xml
<!-- To support in memory database  -->
<dependency>
	<groupId>com.h2database</groupId>
	<artifactId>h2</artifactId>
	<scope>runtime</scope>
</dependency>
<!-- To communicate with database -->
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
```

**Step 3** : Update SecurityConfigurer class as below
```java
@EnableWebSecurity
class SecurityConfigurer extends WebSecurityConfigurerAdapter{
	
	@Autowired
	DataSource dataSource;
	
	/**
	* By overriding this configure method, we are getting hold to {@AuthenticationManagerBuilder},
	* which we then used to provide list of allowed users. 
	*/
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// Setting the configuration on it using in memory authentication
		
		// dataSource : with dataSource() we have pointed sprint security to preconfigured database 
		// withDefaultSchema : create the schema and users in database - so we dont need to create it explicitly 
		auth.jdbcAuthentication()
			.dataSource(dataSource)
			.withDefaultSchema()
			.withUser(
				User.withUsername("sunil")
				.password("abc")
				.roles("USER")
			)
			.withUser(
				User.withUsername("andrea")
				.password("abc")
				.roles("ADMIN")
			);
	}
	
	/**
	* This method indicates which password encoding is used. 
	* With NoOpPasswordEncoder, there is no encoding used i.e. plain String text is returned 
	* @returns {@PasswordEncoder}
	*/
	@Bean
	public PasswordEncoder getPasswordEncoder(){
		return NoOpPasswordEncoder.getInstance();	
	}
	
	/**
	* By overriding this configure method, we are getting hold to {@HttpSecurity},
	* which we then used to restrict users based on their roles. 
	* Here while setting antMatchers, we set access from most restricted to less restrictive
	*/
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers("/admin").hasAnyRole("ADMIN")
		.antMatchers("/user").hasAnyRole("USER","ADMIN")
		.antMatchers("/").permitAll()
		.and().formLogin();
	}
}
```

**Note** : complete source code for this is at : 
https://bitbucket.org/saibol/spring-security/src/feature/jdbc-auth-default-schema


## How to setup JDBC authentication with Spring Security (by providing expected Schema)

**Step 1**: create sample application with spring security having basic spring authencation and authorization implemented
(Source code for basic code : https://bitbucket.org/saibol/spring-security/src/master/)

**Step 2**: As with this example we creating in memory h2 database. add below dependencies in pom.xml
```xml
<!-- To support in memory database  -->
<dependency>
	<groupId>com.h2database</groupId>
	<artifactId>h2</artifactId>
	<scope>runtime</scope>
</dependency>
<!-- To communicate with database -->
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
```

**Step 3**: Update SecurityConfigurer class as below
```java
@EnableWebSecurity
class SecurityConfigurer extends WebSecurityConfigurerAdapter{
	
	@Autowired
	DataSource dataSource;
	
	/**
	* By overriding this configure method, we are getting hold to {@AuthenticationManagerBuilder},
	* which we then used to provide list of allowed users. 
	*/
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// Setting the configuration on it using in memory authentication
		
		// dataSource : with dataSource() we have pointed sprint security to preconfigured database 
		auth.jdbcAuthentication()
			.dataSource(dataSource);
	}
	
	/**
	* This method indicates which password encoding is used. 
	* With NoOpPasswordEncoder, there is no encoding used i.e. plain String text is returned 
	* @returns {@PasswordEncoder}
	*/
	@Bean
	public PasswordEncoder getPasswordEncoder(){
		return NoOpPasswordEncoder.getInstance();	
	}
	
	/**
	* By overriding this configure method, we are getting hold to {@HttpSecurity},
	* which we then used to restrict users based on their roles. 
	* Here while setting antMatchers, we set access from most restricted to less restrictive
	*/
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers("/admin").hasAnyRole("ADMIN")
		.antMatchers("/user").hasAnyRole("USER","ADMIN")
		.antMatchers("/").permitAll()
		.and().formLogin();
	}
}
```
**Step 4** : create schema.sql with DDL queries in resources directory (Note : spring security expects users and authorities tables)

```sql
create table users(
  username varchar_ignorecase(50) not null primary key,
  password varchar_ignorecase(50) not null,
  enabled boolean not null);

create table authorities (
  username varchar_ignorecase(50) not null,
  authority varchar_ignorecase(50) not null,
  constraint fk_authorities_users foreign key(username) references users(username));
  
create unique index ix_auth_username on authorities (username,authority);
```

**Step 5** : create data.sql with DML queries in resources directory (which contains list of users)
```sql
insert into users (username, password, enabled) 
values ('sunil','abc',true);

insert into users (username, password, enabled) 
values ('andrea','abc',true);

insert into authorities(username, authority)
values ('sunil','USER');

insert into authorities(username, authority)
values ('andrea','ADMIN');
```

**Note** : complete source code for this is at : 
https://bitbucket.org/saibol/spring-security/src/feature/jdbc-auth-by-providing-expected-schema


## How to setup JDBC authentication with Spring Security (by providing external Schema)
 
**Step 1** : create sample application with spring security having basic spring authencation and authorization implemented
(Source code for basic code : https://bitbucket.org/saibol/spring-security/src/master/)
**Step 2** : As with this example we creating in memory h2 database. add below dependencies in pom.xml
```xml
<!-- To support in memory database  -->
<dependency>
	<groupId>com.h2database</groupId>
	<artifactId>h2</artifactId>
	<scope>runtime</scope>
</dependency>
<!-- To communicate with database -->
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
```

**Step 3** : Update SecurityConfigurer class as below
```java
@EnableWebSecurity
class SecurityConfigurer extends WebSecurityConfigurerAdapter{
	
	@Autowired
	DataSource dataSource;
	
	/**
	* By overriding this configure method, we are getting hold to {@AuthenticationManagerBuilder},
	* which we then used to provide list of allowed users. 
	*/
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// dataSource : with dataSource() we have pointed sprint security to preconfigured database 
		auth.jdbcAuthentication()
			.dataSource(dataSource)
			.usersByUsernameQuery("select username, password, enabled from my_users where username = ?")
			.authoritiesByUsernameQuery("select username, authority from my_authorities where username = ?");
	}
	
	/**
	* This method indicates which password encoding is used. 
	* With NoOpPasswordEncoder, there is no encoding used i.e. plain String text is returned 
	* @returns {@PasswordEncoder}
	*/
	@Bean
	public PasswordEncoder getPasswordEncoder(){
		return NoOpPasswordEncoder.getInstance();	
	}
	
	/**
	* By overriding this configure method, we are getting hold to {@HttpSecurity},
	* which we then used to restrict users based on their roles. 
	* Here while setting antMatchers, we set access from most restricted to less restrictive
	*/
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers("/admin").hasAnyRole("ADMIN")
		.antMatchers("/user").hasAnyRole("USER","ADMIN")
		.and().formLogin();
	}
}
```
**Step 4** : create schema.sql with DDL queries in resources directory (Note : spring security expects users and authorities tables) 
We have create 2 tables i.e. my_users and my_authorities

```sql
create table my_users(
  username varchar_ignorecase(50) not null primary key,
  password varchar_ignorecase(50) not null,
  enabled boolean not null);

create table my_authorities (
  username varchar_ignorecase(50) not null,
  authority varchar_ignorecase(50) not null,
  constraint fk_my_authorities_my_users foreign key(username) references my_users(username));
  
create unique index ix_auth_username on my_authorities (username,authority);
```

**Step 5** : create data.sql with DML queries in resources directory (which contains list of users)
```sql
insert into my_users (username, password, enabled) 
values ('sunil','abc',true);

insert into my_users (username, password, enabled) 
values ('andrea','abc',true);

insert into my_authorities(username, authority)
values ('sunil','USER');

insert into my_authorities(username, authority)
values ('andrea','ADMIN');

```

**Note** : complete source code for this is at : 
https://bitbucket.org/saibol/spring-security/src/feature/jdbc-auth-by-providing-external-schema

